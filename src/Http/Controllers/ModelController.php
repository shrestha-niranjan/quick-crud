<?php

namespace Najnarin\QuickCrud\Http\Controllers;

use App\Http\Controllers\Controller;

class ModelController extends Controller
{
    /**
     * Show all the models list
     *
     * @return void
     */
    public function index()
    {
        return view('qcrud::index');
    }

    public function create()
    {
        return view('qcrud::create');
    }
}
