<?php

namespace Najnarin\QuickCrud;

use Illuminate\Support\ServiceProvider;
use Najnarin\QuickCrud\Commands\CrudGenerator;

class QuickCrudServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // registering commands for the packages
        if ($this->app->runningInConsole()) {
            $this->commands([
                CrudGenerator::class
            ]);
        }

        $this->publishes([
            __DIR__.'/config/model.php' => config_path('model.php'),
            __DIR__.'/views' => resource_path('views/vendor/qcrud')
        ]);

        $this->publishes([
            __DIR__.'/config/crud.php' => config_path('crud.php'),
        ], 'crud');

        $this->loadRoutesFrom(__DIR__ . "/routes/web.php");
        $this->loadViewsFrom(__DIR__ . "/views", "qcrud");
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->mergeConfigFrom(
            __DIR__ . '/config/model.php',
            'model'
        );
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
