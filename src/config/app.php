<?php

return [
    'providers' => [
    // ...
    'Collective\Html\HtmlServiceProvider',
    // ...
  ],
  'aliases' => [
    // ...
      'Form' => 'Collective\Html\FormFacade',
      'Html' => 'Collective\Html\HtmlFacade',
    // ...
    ],
];
