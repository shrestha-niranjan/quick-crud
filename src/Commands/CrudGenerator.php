<?php

namespace Najnarin\QuickCrud\Commands;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;

class CrudGenerator extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:crud
                            {name : Module name}
                            {--route= : Custom route name}
                            {--m|migration : Add a migration.}
                            {--c|controller : Add a controller}
                            {--r|resource : Generates resourceful controller.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates new CRUD operation for the given arguement.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Running CRUD Generator');


        $this->table = $this->getNameInput();

        // update web.php for the routes
        // $webPath = base_path('routes/web.php');

        // if (Route::has('users' . '[.*]')) {
        //     dd('meet');
        // }

        // dd(file_get_contents($webPath));

        // If table not exist in DB return
        if (!$this->tableExists()) {
            $this->error("`{$this->table}` table not exist");

            return false;
        }

        // Build the class name from table name
        $this->name = $this->_buildClassName();

        // Generate the crud
        $this->buildOptions()
            ->appendRoutes()
        ->buildController()
        ->buildModel()
        ->buildViews();

        $this->info('Created Successfully.');

        return true;
    }

    /**
     * append the routes in web.php if the routes are not present
     */
    protected function appendRoutes()
    {
        if ($this->checkWhetherRouteExists() && $this->ask('Route already exist. Do you want to overwrite it (y/n) ?', 'y') == 'n') {
            return $this;
        }

        $this->info('Creating Routes ...');
    }

    /**
     * Check whether route exists or not
     *
     * @return bool
     */
    private function checkWhetherRouteExists()
    {
        $routeFirstName = [];
        foreach ($this->getRouteList() as $key => $value) {
            $exp = explode('.', $key);
            array_push($routeFirstName, $exp[0]);
        }

        return in_array(strtolower($this->name), $routeFirstName);
    }

    /**
     *  Provides routes presented by the system
     *
     * @return array
     */
    protected function getRouteList()
    {
        $routes = Route::getRoutes()->get();

        $hiddenRoutes = [
            'login',
            // 'role',
            // 'role.index',
            // 'role.create',
            // 'role.destroy',
            // 'role.show',
            // 'role.update',
            // 'role.store',
            'dashboard',
            'logout',
            'role.edit',
            'livewire.message',
            'livewire.preview-file',
            'livewire.upload-file',
            'ignition.healthCheck',
            'ignition.executeSolution',
            'ignition.shareReport',
            'ignition.scripts',
            'ignition.styles',
        ];

        foreach ($routes as $route) {
            if ($route->getName() != null && !in_array($route->getName(), $hiddenRoutes)) {
                $list = str_replace('.', ' ', $route->getName());
                $routeList[$route->getName()] = ucfirst(str_replace(
                    'destroy',
                    'delete',
                    str_replace('index', 'list', $list)
                ));
            }
        }

        return $routeList;
    }

    /**
     * Build the Controller Class and save in app/Http/Controllers.
     *
     * @return $this
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     *
     */
    protected function buildController()
    {
        $controllerPath = $this->_getControllerPath($this->name);

        if (file_exists($controllerPath) &&
            $this->ask('Already exist Controller. Do you want overwrite (y/n)?', 'y') == 'n') {
            return $this;
        }

        $this->info('Creating Controller ...');

        $replace = $this->buildReplacements();

        $controllerTemplate = str_replace(
            array_keys($replace),
            array_values($replace),
            $this->getStub('Controller')
        );

        $this->write($controllerPath, $controllerTemplate);

        return $this;
    }

    /**
     * Make the class name from table name.
     *
     * @return string
     */
    private function _buildClassName()
    {
        return Str::studly(Str::singular($this->table));
    }

    /**
     * Build the replacement.
     *
     * @return array
     */
    protected function buildReplacements()
    {
        return [
            '{{layout}}' => $this->layout,
            '{{modelName}}' => $this->name,
            '{{modelTitle}}' => Str::title(Str::snake($this->name, ' ')),
            '{{modelNamespace}}' => $this->modelNamespace,
            '{{controllerNamespace}}' => $this->controllerNamespace,
            '{{modelNamePluralLowerCase}}' => Str::camel(Str::plural($this->name)),
            '{{modelNamePluralUpperCase}}' => ucfirst(Str::plural($this->name)),
            '{{modelNameLowerCase}}' => Str::camel($this->name),
            '{{modelRoute}}' => $this->options['route'] ?? Str::kebab(Str::plural($this->name)),
            '{{modelView}}' => Str::kebab($this->name),
        ];
    }

    /**
     * Build a model
     *
     * @return $this
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     *
     */
    protected function buildModel()
    {
        $modelPath = $this->_getModelPath($this->name);

        if (\File::exists($modelPath) && $this->ask('Already exist Model. Do you want overwrite (y/n)?', 'y') == 'n') {
            return $this;
        }

        $this->info('Creating Model ...');

        // Make the models attributes and replacement
        $replace = array_merge($this->buildReplacements(), $this->modelReplacements());

        $modelTemplate = str_replace(
            array_keys($replace),
            array_values($replace),
            $this->getStub('Model')
        );

        $this->write($modelPath, $modelTemplate);

        return $this;
    }

    /**
     * @return $this
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     *
     * @throws \Exception
     */
    protected function buildViews()
    {
        $this->info('Creating Views ...');

        $tableHead = "\n";
        $tableBody = "\n";
        $viewRows = "\n";
        $form = "\n";

        foreach ($this->getFilteredColumns() as $column) {
            $title = Str::title(str_replace('_', ' ', $column));

            $tableHead .= $this->getHead($title);
            $tableBody .= $this->getBody($column);
            $viewRows .= $this->getField($title, $column, 'view-field');
            $form .= $this->getField($title, $column, 'form-field');
        }

        $replace = array_merge($this->buildReplacements(), [
            '{{tableHeader}}' => $tableHead,
            '{{tableBody}}' => $tableBody,
            '{{viewRows}}' => $viewRows,
            '{{form}}' => $form,
        ]);

        $this->buildLayout();

        foreach (['index', 'create', 'edit', 'form', 'show'] as $view) {
            $viewTemplate = str_replace(
                array_keys($replace),
                array_values($replace),
                $this->getStub("views/{$view}")
            );

            $this->write($this->_getViewPath($view), $viewTemplate);
        }

        return $this;
    }
}
